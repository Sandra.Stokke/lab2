package INF101.lab2;

import java.util.ArrayList;
import java.util.List;

public class Fridge implements IFridge{
    int max_size = 20;
    ArrayList<FridgeItem> Items = new ArrayList<FridgeItem>();

    public int totalSize() {
        return max_size;
    }
    @Override
    public int nItemsInFridge() {
        return Items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (Items.size()>= max_size)
            return false;
        else Items.add(item);
                return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge()> 0)
            Items.remove(item);
        else
            throw new java.util.NoSuchElementException();

    }

    @Override
    public void emptyFridge() {
        Items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem i : Items) {
            if (i.hasExpired())
                expiredFood.add(i);
        }
        Items.removeAll(expiredFood);
        return expiredFood;
    }
}
